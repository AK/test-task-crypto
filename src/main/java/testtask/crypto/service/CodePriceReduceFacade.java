package testtask.crypto.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CodePriceReduceFacade {
    private final CodePriceReduceService codePriceReduceService;

    public List<CodePrice> process(ReduceRequest reduceRequest) {
        return codePriceReduceService.process(reduceRequest);
    }
}
