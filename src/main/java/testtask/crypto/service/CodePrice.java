package testtask.crypto.service;

import lombok.*;

import java.math.BigDecimal;

/**
 * Represent a pair of a code and a price.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CodePrice {
    private String code;
    private BigDecimal price;
}
