package testtask.crypto.service;

import java.util.List;

/**
 * Processes aggregation request
 */
public interface CodePriceReduceService {
    List<CodePrice> process(ReduceRequest reduceRequest);
}
