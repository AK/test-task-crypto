package testtask.crypto.service;

public enum ReduceFunctionType {
    OLDEST, NEWEST, MIN, MAX, NORMALIZED
}
