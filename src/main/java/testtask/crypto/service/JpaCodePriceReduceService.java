package testtask.crypto.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.math.RoundingMode;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class JpaCodePriceReduceService implements CodePriceReduceService {
    private final EntityManager entityManager;

    @Override
    public List<CodePrice> process(ReduceRequest reduceRequest) {
        return getQuery(reduceRequest).getResultList()
                .stream()
                .peek(cp -> cp.setPrice(cp.getPrice().setScale(2, RoundingMode.HALF_EVEN)))
                .toList();
    }

    private TypedQuery<CodePrice> getQuery(ReduceRequest reduceRequest) {
        return entityManager.createQuery(toSqlQuery(reduceRequest), CodePrice.class)
                .setParameter("start", reduceRequest.getStart())
                .setParameter("end", reduceRequest.getEnd())
                .setParameter("code", reduceRequest.getCode());
    }
    private String toSqlQuery(ReduceRequest reduceRequest) {
        String priceColumnAggregate = "";
        String timeStampCondition = "";
        if (reduceRequest.getReduceOperation() == ReduceFunctionType.MAX
                || reduceRequest.getReduceOperation() == ReduceFunctionType.OLDEST
                || reduceRequest.getReduceOperation() == ReduceFunctionType.NEWEST
        ) {
            priceColumnAggregate = "MAX(cpr.price)";
            if (reduceRequest.getReduceOperation() == ReduceFunctionType.OLDEST) {
                timeStampCondition = "AND cpr.timestamp = (SELECT MIN(t.timestamp) FROM CodePriceRecord t WHERE t.code = cpr.code AND (:start IS NULL OR t.timestamp >= :start) AND (:end IS NULL OR t.timestamp <= :end) ) ";
            } else if (reduceRequest.getReduceOperation() == ReduceFunctionType.NEWEST) {
                timeStampCondition = "AND cpr.timestamp = (SELECT MAX(t.timestamp) FROM CodePriceRecord t WHERE t.code = cpr.code AND (:start IS NULL OR t.timestamp >= :start) AND (:end IS NULL OR t.timestamp <= :end) ) ";
            }
        } else if (reduceRequest.getReduceOperation() == ReduceFunctionType.MIN) {
            priceColumnAggregate = "MIN(cpr.price)";
        } else if (reduceRequest.getReduceOperation() == ReduceFunctionType.NORMALIZED) {
            priceColumnAggregate = "((MAX(cpr.price) - MIN(cpr.price)) / MIN(cpr.price))";
        }

        String query =
                "SELECT NEW testtask.crypto.service.CodePrice(" +
                        "cpr.code, " +
                        priceColumnAggregate + " as price " +
                        ") FROM CodePriceRecord cpr " +
                        "WHERE 1=1 " +
                        "AND (:start IS NULL OR cpr.timestamp >= :start) " +
                        "AND (:end IS NULL OR cpr.timestamp <= :end) " +
                        "AND (:code IS NULL OR cpr.code = :code) " +
                        "AND cpr.price > 0 " +
                        timeStampCondition +
                        "GROUP BY cpr.code " +
                        "ORDER BY " + priceColumnAggregate + " DESC"
                ;

        log.debug(query);

        return query;
    }
}
