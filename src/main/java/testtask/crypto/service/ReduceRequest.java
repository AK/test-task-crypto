package testtask.crypto.service;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
public class ReduceRequest {
    @Pattern(regexp = "^[a-zA-Z]{3}$")
    private String code;
    private Long start;
    private Long end;
    @NotNull
    private ReduceFunctionType reduceOperation;
}
