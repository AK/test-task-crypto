package testtask.crypto.rest;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import testtask.crypto.service.CodePrice;
import testtask.crypto.service.CodePriceReduceFacade;
import testtask.crypto.service.ReduceRequest;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class AggregationController {
    private final CodePriceReduceFacade codePriceReduceFacade;

    @GetMapping("/code-price-record/reduce")
    public List<CodePrice> find(@Valid ReduceRequest reduceRequest) {
        return codePriceReduceFacade.process(reduceRequest);
    }
}
