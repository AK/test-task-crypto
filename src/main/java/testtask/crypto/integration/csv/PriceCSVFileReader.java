package testtask.crypto.integration.csv;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import testtask.crypto.core.domain.CodePriceRecord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;

@Component
public class PriceCSVFileReader {
    @SneakyThrows
    public List<CodePriceRecord> toCodePriceRecords(File file)
    {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            return new CsvToBeanBuilder<CodePriceRecord>(br)
                    .withMappingStrategy(getMappingStrategy())
                    .build()
                    .stream()
                    .toList();
        }
    }

    private ColumnPositionMappingStrategy<CodePriceRecord> getMappingStrategy() {
        ColumnPositionMappingStrategy<CodePriceRecord> mappingStrategy = new ColumnPositionMappingStrategy<>();
        mappingStrategy.setColumnMapping("timestamp", "code" ,"price");
        mappingStrategy.setType(CodePriceRecord.class);
        return mappingStrategy;
    }
}
