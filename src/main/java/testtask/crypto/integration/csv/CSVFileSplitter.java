package testtask.crypto.integration.csv;

import lombok.RequiredArgsConstructor;
import org.springframework.integration.splitter.AbstractMessageSplitter;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import testtask.crypto.core.domain.CodePriceRecord;

import java.io.File;
import java.util.List;
import java.util.Objects;

/**
 * Splits CSV file into code price records.
 */
@Component
@RequiredArgsConstructor
public class CSVFileSplitter extends AbstractMessageSplitter {
    private final PriceCSVFileReader priceCSVFileReader;
    @Override
    protected Object splitMessage(Message<?> message) {
        Object payload = message.getPayload();
        Assert.isInstanceOf(File.class, payload, "Expected java.io.File in the message payload");
        List<CodePriceRecord> records = priceCSVFileReader.toCodePriceRecords((File) payload);
        ((File) Objects.requireNonNull(message.getHeaders().get("file_originalFile"))).delete();
        return records;
    }
}
