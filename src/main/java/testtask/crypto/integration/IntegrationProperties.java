package testtask.crypto.integration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "crypto.integration")
public class IntegrationProperties {
    private String inboundFolder;
    private String fileNamePattern;
}
