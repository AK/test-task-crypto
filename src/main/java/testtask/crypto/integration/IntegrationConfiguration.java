package testtask.crypto.integration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.endpoint.AbstractMessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.AcceptOnceFileListFilter;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.FileListFilter;
import org.springframework.integration.file.filters.RegexPatternFileListFilter;
import testtask.crypto.core.domain.CodePriceRecord;
import testtask.crypto.core.repository.CodePriceRecordRepository;
import testtask.crypto.integration.csv.CSVFileSplitter;

import java.io.File;

@Slf4j
@Configuration
@AllArgsConstructor
public class IntegrationConfiguration {
    public final IntegrationProperties integrationProperties;

    @Bean
    public AbstractMessageSource<File> csvFilesSource(FileListFilter<File> fileFilter) {
        FileReadingMessageSource fileReadingMessageSource = new FileReadingMessageSource();
        fileReadingMessageSource.setDirectory(new File(integrationProperties.getInboundFolder()));
        fileReadingMessageSource.setFilter(fileFilter);
        fileReadingMessageSource.setAutoCreateDirectory(true);
        return fileReadingMessageSource;
    }

    @Bean
    public FileListFilter<File> fileFilter() {
        CompositeFileListFilter<File> compositeFileListFilter = new CompositeFileListFilter<>();
        compositeFileListFilter.addFilter(new AcceptOnceFileListFilter<>());
        compositeFileListFilter.addFilter(new RegexPatternFileListFilter(integrationProperties.getFileNamePattern()));
        return compositeFileListFilter;
    }

    @Bean
    public IntegrationFlow integrationFlow(AbstractMessageSource<File> csvFilesSource,
                                           CSVFileSplitter csvFileSplitter,
                                           CodePriceRecordRepository codePriceRecordRepository) {
        return IntegrationFlows
                .from(csvFilesSource, configurer -> configurer.poller(Pollers.fixedDelay(100)))
                .channel("csvFilesChannel")
                .split(csvFileSplitter)
                .handle(f -> {
                    codePriceRecordRepository.save((CodePriceRecord) f.getPayload());
                })
                .get();
    }
}
