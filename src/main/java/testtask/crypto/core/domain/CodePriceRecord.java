package testtask.crypto.core.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Represents price for the currency specified by code at specific point of time
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(CodePriceRecord.NaturalKey.class)
public class CodePriceRecord {
    @Id
    @Column(name = "ts")
    private Long timestamp;
    @Id
    private String code;
    private BigDecimal price;

    @Data
    public static class NaturalKey implements Serializable {
        private Long timestamp;
        private String code;
    }
}
