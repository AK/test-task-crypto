package testtask.crypto.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import testtask.crypto.core.domain.CodePriceRecord;

@Repository
public interface CodePriceRecordRepository extends CrudRepository<CodePriceRecord, CodePriceRecord.NaturalKey> {
}
