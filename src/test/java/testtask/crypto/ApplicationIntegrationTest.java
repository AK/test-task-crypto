package testtask.crypto;

import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import testtask.crypto.fixtures.CSVFilesFixture;
import testtask.crypto.integration.IntegrationProperties;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = Application.class)
public class ApplicationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private IntegrationProperties integrationProperties;

    @BeforeAll
    public void setUp() {
        CSVFilesFixture.cleanUpInboundFolder();
        assertNoData();
        /*
            1641009400000,BTC,50.00
            1641009600000,BTC,200.00
            1641009700000,BTC,30.00
            1641009500000,BTC,100.00
         */
        CSVFilesFixture.loadFile("btc_values.csv");
        /*
            1641009600000,ETH,200.01
            1641009500000,ETH,300.22
            1641009550000,ETH,0.00
         */
        CSVFilesFixture.loadFile("eth_values.csv");
    }

    @AfterAll
    public void tearDown() {
        CSVFilesFixture.cleanUpInboundFolder();
    }

    @Test
    public void reduceMax() throws Exception {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "MAX")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("ETH"))
                .andExpect(jsonPath("$[0].price").value("300.22"))
                .andExpect(jsonPath("$[1].code").value("BTC"))
                .andExpect(jsonPath("$[1].price").value("200.0"));
    }

    @Test
    public void reduceMinInPeriodWithCode() throws Exception {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "MIN")
                        .param("code", "BTC")
                        .param("start", "1641009500000")
                        .param("end", "1641009600000")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("BTC"))
                .andExpect(jsonPath("$[0].price").value("100.0"))
                .andExpect(jsonPath("$[1]").doesNotExist());
    }

    @Test
    public void reduceMin() throws Exception {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "MIN")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("ETH"))
                .andExpect(jsonPath("$[0].price").value("200.01"))
                .andExpect(jsonPath("$[1].code").value("BTC"))
                .andExpect(jsonPath("$[1].price").value("30.0"));
    }

    @Test
    public void reduceNormalized() throws Exception {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "NORMALIZED")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("BTC"))
                .andExpect(jsonPath("$[0].price").value("5.67"))
                .andExpect(jsonPath("$[1].code").value("ETH"))
                .andExpect(jsonPath("$[1].price").value("0.5"));
    }

    @Test
    public void reduceOldest() throws Exception {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "OLDEST")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("ETH"))
                .andExpect(jsonPath("$[0].price").value("300.22"))
                .andExpect(jsonPath("$[1].code").value("BTC"))
                .andExpect(jsonPath("$[1].price").value("50.0"));
    }

    @Test
    public void reduceOldestWithStart() throws Exception {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "OLDEST")
                        .param("start", "1641009450000")
                        .param("code", "BTC")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("BTC"))
                .andExpect(jsonPath("$[0].price").value("100.0"));
    }

    @Test
    public void reduceNewest() throws Exception {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "NEWEST")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("ETH"))
                .andExpect(jsonPath("$[0].price").value("200.01"))
                .andExpect(jsonPath("$[1].code").value("BTC"))
                .andExpect(jsonPath("$[1].price").value("30.0"));
    }

    @SneakyThrows
    private void assertNoData() {
        mockMvc.perform(get("/api/v1/code-price-record/reduce")
                        .param("reduceOperation", "MAX"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }
}
