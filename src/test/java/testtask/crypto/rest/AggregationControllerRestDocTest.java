package testtask.crypto.rest;

import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import testtask.crypto.service.CodePrice;
import testtask.crypto.service.CodePriceReduceFacade;
import testtask.crypto.service.ReduceFunctionType;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.snippet.Attributes.attributes;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class AggregationControllerRestDocTest {

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private WebApplicationContext context;
    @MockBean
    private CodePriceReduceFacade codePriceReduceFacade;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                .build();
    }

    @Test
    public void reduce() throws Exception {
        doReturn(Arrays.asList(
                        CodePrice
                                .builder()
                                .code("BTC")
                                .price(BigDecimal.valueOf(12456, 2))
                                .build(),
                        CodePrice
                                .builder()
                                .code("ETH")
                                .price(BigDecimal.valueOf(7890, 2))
                                .build()
                )
        ).when(codePriceReduceFacade).process(any());

        mockMvc.perform(
                        RestDocumentationRequestBuilders.
                                get("/api/v1/code-price-record/reduce")
                                .param("reduceOperation", "MIN")
                                .param("code", "BTC")
                                .param("start", "1641009500000")
                                .param("end", "1641009600000")

                )
                .andExpect(status().isOk())
                .andDo(document("codePriceRecordReduceSnippets",
                                requestParameters(
                                        attributes(key("title").value("Aggregation parameters")),
                                        parameterWithName("reduceOperation")
                                                .description("Aggregation function name")
                                                .attributes(key("required").value(Boolean.TRUE))
                                                .attributes(key("values").value(
                                                        Arrays.stream(ReduceFunctionType.values())
                                                                .map(Enum::name)
                                                                .collect(Collectors.joining(", ")) + " see <<aggregate-functions,Aggregate functions>>")
                                                )
                                                .attributes(key("example").value("MAX")),
                                        parameterWithName("code")
                                                .description("Currency code. When not specified aggregation is applied to all available currency codes")
                                                .attributes(key("required").value(Boolean.FALSE))
                                                .attributes(key("values").value("3 character string"))
                                                .attributes(key("example").value("BTC")),
                                        parameterWithName("start")
                                                .description("Lowest (oldest) timestamp for the time frame to which aggregation to be applied. When not specified no timestamp filtering for lower boundary is applied.")
                                                .attributes(key("required").value(Boolean.FALSE))
                                                .attributes(key("values").value("Unix timestamp"))
                                                .attributes(key("example").value("1641009500000")),
                                        parameterWithName("end")
                                                .description("Highest (newest) timestamp for the time frame to which aggregation to be applied. When not specified no timestamp filtering for higher boundary is applied.")
                                                .attributes(key("required").value(Boolean.FALSE))
                                                .attributes(key("values").value("Unix timestamp"))
                                                .attributes(key("example").value("1641009500000"))
                                )
                        )
                )
        ;
    }
}