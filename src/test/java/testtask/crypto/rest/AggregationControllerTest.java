package testtask.crypto.rest;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import testtask.crypto.service.CodePrice;
import testtask.crypto.service.CodePriceReduceFacade;
import testtask.crypto.service.ReduceFunctionType;
import testtask.crypto.service.ReduceRequest;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class AggregationControllerTest {
    @Autowired
    private WebApplicationContext context;
    @MockBean
    private CodePriceReduceFacade codePriceReduceFacade;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext) {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test @SneakyThrows
    public void reduce_whenAllFieldsProvided_returns200() {
        mockFacade(ReduceRequest.builder()
                .code("BTC")
                .start(1641009500000L)
                .end(1641009600000L)
                .reduceOperation(ReduceFunctionType.MIN)
                .build());

        mockMvc.perform(
                        RestDocumentationRequestBuilders.
                                get("/api/v1/code-price-record/reduce")
                                .param("reduceOperation", "MIN")
                                .param("code", "BTC")
                                .param("start", "1641009500000")
                                .param("end", "1641009600000")

                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("BTC"))
                .andExpect(jsonPath("$[0].price").value("124.56"))
        ;
    }

    @Test @SneakyThrows
    public void reduce_whenOnlyReduceOperationProvided_returns200() {
        mockFacade(ReduceRequest.builder()
                .reduceOperation(ReduceFunctionType.MIN)
                .build());

        mockMvc.perform(
                        RestDocumentationRequestBuilders.
                                get("/api/v1/code-price-record/reduce")
                                .param("reduceOperation", "MIN")

                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].code").value("BTC"))
                .andExpect(jsonPath("$[0].price").value("124.56"))
        ;
    }

    @Test @SneakyThrows
    public void reduce_whenReduceOperationNotProvided_returns400() {
        mockMvc.perform(
                        RestDocumentationRequestBuilders.
                                get("/api/v1/code-price-record/reduce")

                )
                .andExpect(status().isBadRequest())
        ;
    }

    @Test @SneakyThrows
    public void reduce_whenCodeIsInvalid_returns400() {
        mockMvc.perform(
                        RestDocumentationRequestBuilders.
                                get("/api/v1/code-price-record/reduce")
                                .param("reduceOperation", "MIN")
                                .param("code", "BTCA")

                )
                .andExpect(status().isBadRequest())
        ;
    }

    private void mockFacade(ReduceRequest reduceRequest) {
        doReturn(Arrays.asList(CodePrice
                .builder()
                .code("BTC")
                .price(BigDecimal.valueOf(12456, 2))
                .build())
        ).when(codePriceReduceFacade).process(reduceRequest);
    }
}