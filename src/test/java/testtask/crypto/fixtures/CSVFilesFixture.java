package testtask.crypto.fixtures;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import testtask.crypto.ApplicationIntegrationTest;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class CSVFilesFixture {

    //TODO: read it from pops integrationProperties.getInboundFolder()
    private static final String inboundFolder = "./data/inbound"; // integrationProperties.getInboundFolder()

    @SneakyThrows
    public static File getTestFile(String fileName) {
        return new File(ApplicationIntegrationTest.class.getClassLoader().getResource("fixtures/" + fileName).toURI());
    }

    @SneakyThrows
    public static void cleanUpInboundFolder() {
        FileUtils.cleanDirectory(new File(inboundFolder));
    }

    public static void loadFile(String fileName) {
        cleanUpInboundFolder();
        waitForFileDisappear(
                copyFileToInboundFolder(fileName)
        );
    }

    @SneakyThrows
    private static File copyFileToInboundFolder(String fileName) {
        FileUtils.cleanDirectory(new File(inboundFolder));
        File file = getTestFile(fileName);
        File newFile = new File( inboundFolder + "/" + fileName);
        Files.copy(file.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        return newFile;
    }

    @SneakyThrows
    private static void waitForFileDisappear(File file) {
        int delay = 100;
        int timeout = 120000;
        int timeSpent = 0;
        while (timeSpent < timeout) {
            if (!file.exists()) {
                return;
            }
            Thread.sleep(delay);
            timeSpent += delay;
        }
        throw new RuntimeException(String.format("Timeout wait for file to disappear %s", file.getAbsoluteFile()));
    }
}
