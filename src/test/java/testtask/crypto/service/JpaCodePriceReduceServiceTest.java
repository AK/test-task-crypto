package testtask.crypto.service;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class JpaCodePriceReduceServiceTest {

    @Mock
    private EntityManager entityManager;
    @Mock
    private TypedQuery<CodePrice> query;
    @InjectMocks
    private JpaCodePriceReduceService jpaCodePriceReduceService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(query.setParameter("start", 1L)).thenReturn(query);
        when(query.setParameter("end", 2L)).thenReturn(query);
        when(query.setParameter("code", "C")).thenReturn(query);
        CodePrice codePrice = CodePrice.builder()
                .code("A")
                .price(BigDecimal.valueOf(2000000000001L, 10))
                .build();
        when(query.getResultList()).thenReturn(Lists.newArrayList(codePrice));
    }

    @Test
    public void process_whenMax() {
        when(entityManager.createQuery(
                "SELECT " +
                        "NEW testtask.crypto.service.CodePrice(" +
                        "cpr.code, " +
                        "MAX(cpr.price) as price ) " +
                        "FROM CodePriceRecord cpr " +
                        "WHERE 1=1 " +
                        "AND (:start IS NULL OR cpr.timestamp >= :start) " +
                        "AND (:end IS NULL OR cpr.timestamp <= :end) " +
                        "AND (:code IS NULL OR cpr.code = :code) " +
                        "AND cpr.price > 0 " +
                        "GROUP BY cpr.code " +
                        "ORDER BY MAX(cpr.price) DESC",
                CodePrice.class)).thenReturn(query);

        List<CodePrice> actual = jpaCodePriceReduceService.process(ReduceRequest.builder()
                .reduceOperation(ReduceFunctionType.MAX)
                .start(1L)
                .end(2L)
                .code("C")
                .build());

        assertActual(actual);
    }

    @Test
    public void process_whenMin() {
        when(entityManager.createQuery(
                "SELECT " +
                        "NEW testtask.crypto.service.CodePrice(" +
                        "cpr.code, " +
                        "MIN(cpr.price) as price ) " +
                        "FROM CodePriceRecord cpr " +
                        "WHERE 1=1 " +
                        "AND (:start IS NULL OR cpr.timestamp >= :start) " +
                        "AND (:end IS NULL OR cpr.timestamp <= :end) " +
                        "AND (:code IS NULL OR cpr.code = :code) " +
                        "AND cpr.price > 0 " +
                        "GROUP BY cpr.code " +
                        "ORDER BY MIN(cpr.price) DESC",
                CodePrice.class)).thenReturn(query);

        List<CodePrice> actual = jpaCodePriceReduceService.process(ReduceRequest.builder()
                .reduceOperation(ReduceFunctionType.MIN)
                .start(1L)
                .end(2L)
                .code("C")
                .build());

        assertActual(actual);
    }

    @Test
    public void process_whenNormalized() {
        when(entityManager.createQuery(
                "SELECT NEW testtask.crypto.service.CodePrice(" +
                        "cpr.code, " +
                        "((MAX(cpr.price) - MIN(cpr.price)) / MIN(cpr.price)) as price ) " +
                        "FROM CodePriceRecord cpr " +
                        "WHERE 1=1 " +
                        "AND (:start IS NULL OR cpr.timestamp >= :start) " +
                        "AND (:end IS NULL OR cpr.timestamp <= :end) " +
                        "AND (:code IS NULL OR cpr.code = :code) " +
                        "AND cpr.price > 0 " +
                        "GROUP BY cpr.code " +
                        "ORDER BY ((MAX(cpr.price) - MIN(cpr.price)) / MIN(cpr.price)) DESC",
                CodePrice.class)).thenReturn(query);

        List<CodePrice> actual = jpaCodePriceReduceService.process(ReduceRequest.builder()
                .reduceOperation(ReduceFunctionType.NORMALIZED)
                .start(1L)
                .end(2L)
                .code("C")
                .build());

        assertActual(actual);
    }

    @Test
    public void process_whenOldest() {
        when(entityManager.createQuery(
                "SELECT " +
                        "NEW testtask.crypto.service.CodePrice(" +
                        "cpr.code, " +
                        "MAX(cpr.price) as price ) " +
                        "FROM CodePriceRecord cpr " +
                        "WHERE 1=1 " +
                        "AND (:start IS NULL OR cpr.timestamp >= :start) " +
                        "AND (:end IS NULL OR cpr.timestamp <= :end) " +
                        "AND (:code IS NULL OR cpr.code = :code) " +
                        "AND cpr.price > 0 " +
                        "AND cpr.timestamp = (" +
                        "SELECT MIN(t.timestamp) " +
                        "FROM CodePriceRecord t " +
                        "WHERE t.code = cpr.code " +
                        "AND (:start IS NULL OR t.timestamp >= :start) " +
                        "AND (:end IS NULL OR t.timestamp <= :end) ) " +
                        "GROUP BY cpr.code " +
                        "ORDER BY MAX(cpr.price) DESC",
                CodePrice.class)).thenReturn(query);

        List<CodePrice> actual = jpaCodePriceReduceService.process(ReduceRequest.builder()
                .reduceOperation(ReduceFunctionType.OLDEST)
                .start(1L)
                .end(2L)
                .code("C")
                .build());

        assertActual(actual);
    }

    @Test
    public void process_whenNewest() {
        when(entityManager.createQuery(
                "SELECT " +
                        "NEW testtask.crypto.service.CodePrice(" +
                        "cpr.code, " +
                        "MAX(cpr.price) as price ) " +
                        "FROM CodePriceRecord cpr " +
                        "WHERE 1=1 " +
                        "AND (:start IS NULL OR cpr.timestamp >= :start) " +
                        "AND (:end IS NULL OR cpr.timestamp <= :end) " +
                        "AND (:code IS NULL OR cpr.code = :code) " +
                        "AND cpr.price > 0 " +
                        "AND cpr.timestamp = (" +
                        "SELECT MAX(t.timestamp) " +
                        "FROM CodePriceRecord t " +
                        "WHERE t.code = cpr.code " +
                        "AND (:start IS NULL OR t.timestamp >= :start) " +
                        "AND (:end IS NULL OR t.timestamp <= :end) ) " +
                        "GROUP BY cpr.code ORDER BY MAX(cpr.price) DESC",
                CodePrice.class)).thenReturn(query);

        List<CodePrice> actual = jpaCodePriceReduceService.process(ReduceRequest.builder()
                .reduceOperation(ReduceFunctionType.NEWEST)
                .start(1L)
                .end(2L)
                .code("C")
                .build());

        assertActual(actual);
    }

    private void assertActual(List<CodePrice> actual) {
        assertThat(actual).containsExactly(CodePrice.builder()
                .code("A")
                .price(BigDecimal.valueOf(20000, 2))
                .build());
    }

}