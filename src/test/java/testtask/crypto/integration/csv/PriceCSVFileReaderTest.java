package testtask.crypto.integration.csv;

import org.junit.jupiter.api.Test;
import testtask.crypto.core.domain.CodePriceRecord;
import testtask.crypto.fixtures.CSVFilesFixture;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PriceCSVFileReaderTest {

    @Test
    public void toCodePriceRecords() {
        File file = CSVFilesFixture.getTestFile("btc_values.csv");
        PriceCSVFileReader priceCSVFileReader = new PriceCSVFileReader();

        List<CodePriceRecord> actual = priceCSVFileReader.toCodePriceRecords(file);

        assertThat(actual).containsExactly(
                CodePriceRecord.builder()
                        .timestamp(1641009400000L)
                        .code("BTC")
                        .price(BigDecimal.valueOf(5000, 2))
                        .build(),
                CodePriceRecord.builder()
                        .timestamp(1641009600000L)
                        .code("BTC")
                        .price(BigDecimal.valueOf(20000, 2))
                        .build(),
                CodePriceRecord.builder()
                        .timestamp(1641009700000L)
                        .code("BTC")
                        .price(BigDecimal.valueOf(3000, 2))
                        .build(),
                CodePriceRecord.builder()
                        .timestamp(1641009500000L)
                        .code("BTC")
                        .price(BigDecimal.valueOf(10000, 2))
                        .build()
        );
    }
}