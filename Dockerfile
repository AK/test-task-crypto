FROM openjdk:17

EXPOSE 8080
RUN mkdir /app
COPY build/libs/test-task-crypto-0.0.1-SNAPSHOT.jar /app/app.jar
CMD java -jar /app/app.jar