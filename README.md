# test-task-crypto

### Build docker image

```shell
./gradlew bootBuildImage
```

### Generate RestDocs

```shell
./gradlew asciidoctor
```

### Install to Kubernetes with Helm

```shell
helm install crypto-app ./helmchart
```

#### Notes:

- rate limit in NGINX config
- time series database 